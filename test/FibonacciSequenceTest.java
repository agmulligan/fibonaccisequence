import FibonacciSequence.FibonacciSequenceClass;
import static org.junit.Assert.*;
import org.junit.Test;

public class FibonacciSequenceTest {

    
    @Test
    public void testInt() {
        FibonacciSequenceClass myUnit = new FibonacciSequenceClass();
        
        int result = myUnit.fibonacci(1);
        assertEquals(0, result);
        
        int result2 = myUnit.fibonacci(2);
        assertEquals(1, result2);
        
        int result3 = myUnit.fibonacci(3);
        assertEquals(1, result3);
        
        int result4 = myUnit.fibonacci(4);
        assertEquals(2, result4);
        
        int result5 = myUnit.fibonacci(12);
        assertEquals(89, result5);
        
        int result6 = myUnit.fibonacci(20);
        assertEquals(4181, result6);
        
        int result7 = myUnit.fibonacci(29);
        assertEquals(317811, result7);
        
        int result8 = myUnit.fibonacci(-1);
        assertEquals(0, result8);
        

    }
}